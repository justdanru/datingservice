body {
  margin: 0;
  line-height: normal;
}

:root {
  /* fonts */
  --font-manrope: Manrope;
/* Colors */
  --color-steelblue: #095c8b;
  --color-white: #fff;
}
