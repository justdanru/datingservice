from flask import Flask
from flask_restful import  Api
from methods.auth import authentication
from methods.reg import registration
from methods.like import like
from methods.search import search
from methods.meet import meet

app = Flask(__name__)
api = Api(app)
api.add_resource(authentication,'/api/v1/auth')
api.add_resource(registration,'/api/v1/reg')
api.add_resource(like,'/api/v1/like')
api.add_resource(search,'/api/v1/search')
api.add_resource(meet,'/api/v1/meet')

if __name__ == '__main__':
    app.run(debug=True)